# Changelog

## [v0.11.2](https://gitea.com/gitea/go-sdk/pulls?q=&type=all&state=closed&milestone=1256) - 2020-03-31
* ENHANCEMENTS
  * On Internal Server Error, show request witch caused this (#297)

## [v0.11.1](https://gitea.com/gitea/go-sdk/pulls?q=&type=all&state=closed&milestone=1235) - 2020-03-29
* BUGFIXES
  * Fix SetRepoTopics (#276) (#274)
  * Fix AddEmail (#260) (#261)
  * Fix UserApp Functions (#247) (#256)
* ENHANCEMENTS
  * Add IssueType as filter for ListIssues (#288)
  * Correct version (#259)

## [v0.11.0](https://gitea.com/gitea/go-sdk/pulls?q=&type=all&state=closed&milestone=1222) - 2020-01-27
* FEATURES
  * Add VersionCheck (#215)
  * Add Issue Un-/Subscription function (#214)
  * Add Reaction struct and functions (#213)
  * Add GetBlob (#212)
* BUGFIXES
  * Fix ListIssue Functions (#225)
  * Fix ListRepoPullRequests (#219)
* ENHANCEMENTS
  * Add some pull list options (#217)
  * Extend StopWatch struct & functions (#211)
* TESTING
  * Add Test Framework (#227)
* BUILD
  * Use golangci-lint and revive for linting (#220)
